#lang racket/base

(require racket/contract/base)


(provide (contract-out [dirnames (-> path-string? (listof string?))]
                       [dirname (-> path-string? string?)]
                       [basename (-> path-string? string?)]))

(define (dirnames pth)
  (define-values (dirname basename _)
    (split-path pth))
  (list (case dirname
          [(#f) "/"]
          [(relative) "."]
          [else (path->string dirname)])
        (case basename
          [(same) "."]
          [(up) ".."]
          [else (path->string basename)])))

(define (dirname pth)
  (car (dirnames pth)))

(define (basename pth)
  (cadr (dirnames pth)))


(module+ test
  (require rackunit)

  (check-equal? (dirname "/") "/")
  (check-equal? (dirname ".") ".")
  (check-equal? (dirname "..") ".")
  (check-equal? (dirname "1") ".")
  (check-equal? (dirname "/1") "/")
  (check-equal? (dirname "/1/2/") "/1/")
  (check-equal? (dirname "/1/2/3") "/1/2/")

  (check-equal? (basename "/") "/")
  (check-equal? (basename ".") ".")
  (check-equal? (basename "..") "..")
  (check-equal? (basename "1") "1")
  (check-equal? (basename "/1/2/3") "3")
  )
