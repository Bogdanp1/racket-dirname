#lang scribble/manual


@(require
   scribble/example
   @(for-label racket dirname))


@(define myeval
   (make-base-eval '(require dirname)))


@title[#:tag "dirname"]{DirName}

@author[@author+email["Maciej Barć" "xgqt@riseup.net"]]


@defmodule[dirname]


@defproc[
 (dirnames
  [pth  (listof path-string?)])
 string?
 ]{
 @examples[
 #:eval myeval
 (dirnames "/home/user/Downloads")
 ]}

@defproc[
 (dirname
  [pth  path-string?])
 string?
 ]{
 @examples[
 #:eval myeval
 (dirname "/home/user/Downloads")
 ]}

@defproc[
 (basename
  [pth  path-string?])
 string?
 ]{
 @examples[
 #:eval myeval
 (basename "/home/user/Downloads")
 ]}
